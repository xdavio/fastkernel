#define BOOST_TEST_MAIN
#if !defined( WIN32 )
    #define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>
#include <shark/LinAlg/Base.h>
#include <Common.h>
#include <hadamardmatrix.h>
#include <shark/Data/Dataset.h>
#include <normal_distribution.h>
#include <CountSketch.h>
#include <TensorSketch.h>
#include <shark/Statistics/Statistics.h>
#include <fastfood.h>
#include <stacker.h>
#include <vector>
#include <TensorSketch.h>
#include <CountSketch.h>


BOOST_AUTO_TEST_CASE ( test1 )
{
    BOOST_CHECK(4 == 4);
}

BOOST_AUTO_TEST_CASE (FastFood_apply_V){
    //Build V manually
    std::vector<double> S1 = {0.5,0.5};
    std::vector<double> G1 = {0.7,-0.3};
    std::vector<int> B1 = {1,-1};
    int P1_seed = 1;
    std::vector<double> S2 = {0.3,0.3};
    std::vector<double> G2 = {-0.5,0.8};
    std::vector<int> B2 = {-1,-1};
    int P2_seed = 11111111111;

    FastFood V1(S1,G1,B1,P1_seed);
    FastFood V2(S2,G2,B2,P2_seed);

    std::vector<FastFood> V;
    V.push_back(V1);
    V.push_back(V2);

    shark::RealVector x(2);
    x[0]=2;
    x[1]=-1;

    FastFoodStacked FF(V);
    //These are the two methods actually being tested:
    shark::RealVector Vx = FF.Vx(x);
    shark::RealVector mapped = FF.normalize_exp(Vx);


    vector<double> expected_result{-0.0707107,0.565685,-0.403051,0.615183};
    vector<double> expected_map{0.997501,0.844222,0.919869,0.816668,-0.0706518,0.535994,-0.392227,0.577108};
    //1/sqrt(d) normalization of cos,sin values in map.
    for(unsigned int i=0;i<expected_map.size();++i){
        expected_map[i] *=0.5;
    }

    for(unsigned int i=0;i<Vx.size();++i){
        BOOST_CHECK_CLOSE(Vx[i],expected_result[i],0.001);
    }
    for(unsigned int i=0;i<mapped.size();++i){
        BOOST_CHECK_CLOSE(mapped[i],expected_map[i],0.001);
    }



}

BOOST_AUTO_TEST_CASE (Count_sketch_SketchVector_precision)
{
    shark::RealVector a(123,2);
    shark::RealVector b(123,4);
    double result = shark::blas::inner_prod(a,b);
    shark::Statistics stats;
    for(int i=0;i<10;++i)
    {
        CountSketch<shark::RealVector> Counter(123,2000);
        shark::RealVector am = Counter.sketchVector(a);
        shark::RealVector bm = Counter.sketchVector(b);
        double output = shark::blas::inner_prod(am,bm);
        stats(output);
    }

    BOOST_CHECK_CLOSE(stats(shark::Statistics::Mean()),result,1);
}

BOOST_AUTO_TEST_CASE (Count_sketch_SketchVector_manual){
    vector<double> input {1,2,3,4};
    CountSketch<vector<double>> CS(4,4);
    vector<double> expected {0,0,0,0};
    map<unsigned int,unsigned int> map_h = CS.getHash_map();
    map<unsigned int,signed int>   map_s = CS.getBool_map();


    for(unsigned int D=0;D<4;++D){
        for(int d=0;d<4;++d){
            if(D==map_h[d]){
                expected[D]+=map_s[d]*input[d];
            }
        }
    }

    vector<double> test = CS.sketchVector(input);

    for(int i=0;i<4;++i){
        BOOST_CHECK_EQUAL(test[i],expected[i]);
    }
}

BOOST_AUTO_TEST_CASE (Tensor_sketch_FFT_Multiplication_2_4){
    //manually find the tensor product.
    shark::RealVector a(2);
    a[0]=1;
    a[1]=2;
    std::vector<double> aa {1,2,2,4};

    //Choose p h and s functions. And find corresponding H,S:
    //Here we assume for H and S that the mapping runs like this:
    //H(0)=h1(0)+h2(0) mod D
    //H(1)=h1(0)+h2(1) mod D
    //H(2)=h1(1)+h2(0) mod D
    //This makes sense when we descruct the matrix into one row vector by pasting rows.

    std::map<unsigned int, unsigned int> H;
    H[0]=3;
    H[1]=1;
    H[2]=1;
    H[3]=3;
    //H should correspond to sum of h1,h2 mod 4;
    std::map<unsigned int, unsigned int> h1;
    std::map<unsigned int, unsigned int> h2;
    h1[0]=0;h2[0]=3;
    h1[1]=2;h2[1]=1;

    std::map<unsigned int, signed int> S;
    S[0]=-1;
    S[1]=-1;
    S[2]=1;
    S[3]=1;


    std::map<unsigned int, signed int> s1;
    std::map<unsigned int, signed int> s2;
    s1[0]=1;  s2[0]=-1;
    s1[1]=-1; s2[1]=-1;

    //Using H and S from above: CS({1,2,2,4}={0,0,0,3}
    std::vector<double> expected={0,0,0,3};
    //Now try computing it using the library

    //1. Build the Tensor Sketcher
    CountSketch<shark::RealVector> sketcher1(2,4,h1,s1);
    CountSketch<shark::RealVector> sketcher2(2,4,h2,s2);
    std::vector<CountSketch<shark::RealVector>> CSstack;
    CSstack.push_back(sketcher1);
    CSstack.push_back(sketcher2);

    TensorSketch TS(2,4,2,CSstack);

    //Sketch
    shark::RealVector test = TS.map(a);


    //Compare that to the mapped results when using the decomposed hashfunctions
    for(int i=0;i<4;++i){
        BOOST_CHECK_EQUAL(test[i],expected[i]);
    }


}

BOOST_AUTO_TEST_CASE (Tensor_sketch_FFT_Multiplication_4_4){
    //manually find the tensor product.
    shark::RealVector a(4);
    a[0]=1;
    a[1]=0.2;
    a[2]=1;
    a[3]=0.5;
    std::vector<double> aa {1   ,0.2  ,1  ,0.5,
                            0.2 ,0.04 ,0.2,0.1,
                            1   ,0.2  ,1  ,0.5,
                            0.5 ,0.1  ,0.5,0.25};

    //Choose p h and s functions. And find corresponding H,S:
    //Here we assume for H and S that the mapping runs like this:
    //H(0)=h1(0)+h2(0) mod D
    //H(1)=h1(0)+h2(1) mod D
    //H(2)=h1(1)+h2(0) mod D
    //This makes sense when we descruct the matrix into one row vector by pasting rows.

    std::map<unsigned int, unsigned int> H;
    H[0]=3;
    H[1]=3;
    H[2]=2;
    H[3]=1;
    H[4]=0;
    H[5]=0;
    H[6]=3;
    H[7]=2;
    H[8]=3;
    H[9]=3;
    H[10]=2;
    H[11]=1;
    H[12]=1;
    H[13]=1;
    H[14]=0;
    H[15]=3;

    //H should correspond to sum of h1,h2 mod 4;
    std::map<unsigned int, unsigned int> h1;
    std::map<unsigned int, unsigned int> h2;
    h1[0]=1;h2[0]=2;
    h1[1]=2;h2[1]=2;
    h1[2]=1;h2[2]=1;
    h1[3]=3;h2[3]=0;

    std::map<unsigned int, signed int> S;
    S[0]=1;
    S[1]=-1;
    S[2]=1;
    S[3]=-1;
    S[4]=-1;
    S[5]=1;
    S[6]=-1;
    S[7]=1;
    S[8]=1;
    S[9]=-1;
    S[10]=1;
    S[11]=-1;
    S[12]=1;
    S[13]=-1;
    S[14]=1;
    S[15]=-1;


    std::map<unsigned int, signed int> s1;
    std::map<unsigned int, signed int> s2;
    s1[0]=1;  s2[0]= 1;
    s1[1]=-1; s2[1]=-1;
    s1[2]=1;  s2[2]= 1;
    s1[3]= 1; s2[3]=-1;

    //Using H and S from above: CS({{1   ,0.2  ,1  ,0.5,
                                   //0.2 ,0.04 ,0.2,0.1,
                                   //1   ,0.2  ,1  ,0.5,
                                   //0.5 ,0.1  ,0.5,0.25}}={0.34,-0.6,2.1,1.15}
    std::vector<double> expected={0.34,-0.6,2.1,1.15};
    //Now try computing it using the library

    //1. Build the Tensor Sketcher
    CountSketch<shark::RealVector> sketcher1(4,4,h1,s1);
    CountSketch<shark::RealVector> sketcher2(4,4,h2,s2);
    std::vector<CountSketch<shark::RealVector>> CSstack;
    CSstack.push_back(sketcher1);
    CSstack.push_back(sketcher2);

    TensorSketch TS(4,4,2,CSstack);

    //Sketch
    shark::RealVector test = TS.map(a);


    //Compare that to the mapped results when using the decomposed hashfunctions
    for(int i=0;i<4;++i){
        BOOST_CHECK_CLOSE(test[i],expected[i],0.001);
    }


}


BOOST_AUTO_TEST_CASE (FastFood_permutation)
{
    shark::RealVector input_I(4,1);
    shark::RealVector input_II(4,2);
    FastFood FF(4);
    input_I[0] = 0;
    input_I[1] = 1;
    input_I[2] = 2;
    input_I[3] = 3;
    input_II[0] = 0;
    input_II[1] = 1;
    input_II[2] = 2;
    input_II[3] = 3;
    FF.permutate(input_I);
    FF.permutate(input_II);
    FF.permutate(input_I);
    FF.permutate(input_II);
    for(int i=0;i<4;++i){
        BOOST_CHECK_EQUAL(input_I[i],input_II[i]);
    }
}

BOOST_AUTO_TEST_CASE ( FWHT_test_unnormalized )
{    
    for(int i=2;i<1025;i*=2){
        Normal_distribution dist(0,1,i);
        shark::Data<shark::RealVector> data = dist.generateDataset(10);
        HadamardMatrix A;
        A.populate(i);

        //elementwise journey through dataset
        typedef shark::Data<shark::RealVector>::element_range Elements;
        Elements elements = data.elements();
        for(Elements::iterator pos = elements.begin(); pos != elements.end(); ++pos){
            shark::RealVector F = common::dataconverter::shark_elementref_to_shark_realVector(*pos);
            A.FWHT(F);
            shark::RealVector S = shark::blas::prod(A.matrix(),*pos);
            //Check that the vectors have the same length
            BOOST_CHECK_EQUAL(F.size(),S.size());
            //Check that the vectors have the same values
            for(int i=0;i<F.size();++i)
            {
                BOOST_CHECK_CLOSE(F[i],S[i],0.00001);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE ( FWHT_test_normalized )
{
    for(int i=2;i<1025;i*=2){
        Normal_distribution dist(0,1,i);
        shark::Data<shark::RealVector> data = dist.generateDataset(10);
        HadamardMatrix A;
        A.populate_n(i);

        //elementwise journey through dataset
        typedef shark::Data<shark::RealVector>::element_range Elements;
        Elements elements = data.elements();
        for(Elements::iterator pos = elements.begin(); pos != elements.end(); ++pos){
            shark::RealVector F = common::dataconverter::shark_elementref_to_shark_realVector(*pos);
            A.FWHTn(F);
            shark::RealVector S = shark::blas::prod(A.matrix(),*pos);
            //Check that the vectors have the same length
            BOOST_CHECK_EQUAL(F.size(),S.size());
            //Check that the vectors have the same values
            for(int i=0;i<F.size();++i)
            {
                BOOST_CHECK_CLOSE(F[i],S[i],0.00001);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE( converter_test)
{
    std::vector<double> stlvector {0,1,2,3,4,5,6,7,8,9};
    shark::RealVector realvector = common::dataconverter::stl_vector_to_shark_RealVector(stlvector);
    BOOST_CHECK_EQUAL(stlvector.size(),realvector.size());
    for(int i=0;i<stlvector.size();++i)
    {
        BOOST_CHECK(stlvector[i] == realvector[i]);
    }
    std::vector<double> stl2 = common::dataconverter::shark_RealVector_to_stl_vector(realvector);
    BOOST_CHECK_EQUAL(stl2.size(),realvector.size());
    for(int i=0;i<stlvector.size();++i)
    {
        BOOST_CHECK(stl2[i] == realvector[i]);
    }

}

BOOST_AUTO_TEST_CASE (Tensor_Sketch_Sketch_precision){
    shark::RealVector a(4);
    a[0]=1;
    a[1]=0.2;
    a[2]=1;
    a[3]=0.5;


    shark::Statistics stats;

    for(int i=0;i<100;++i){
        TensorSketch TS(4,4096,2);
        double prod = shark::blas::inner_prod(TS.map(a),TS.map(a));
        stats(prod);
    }

    //<a^(2),a^(2)>=<a,a>^2=5.2441
    BOOST_CHECK_CLOSE(stats(shark::Statistics::Mean()),std::pow(shark::blas::inner_prod(a,a),2),1);

}
