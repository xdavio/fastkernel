project(client)
cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3")

#Flags -O3 for optimize

#Does this project need TensorSkecthing?
option (USE_TS "Use Tensor Sketching implementation" ON)

if(USE_TS)
    include_directories("${client_SOURCE_DIR}/../../TensorSketch")
    add_subdirectory("${client_SOURCE_DIR}/../../TensorSketch" "${client_SOURCE_DIR}/TensorSketch")
    set (TS_LIBS ${TS_LIBS} TensorSketch)
endif(USE_TS)

option (USE_VT "Use custom Linear Algebra tools" ON)

if(USE_VT)
    include_directories("${client_SOURCE_DIR}/../../common")
    add_subdirectory("${client_SOURCE_DIR}/../../common" "${client_SOURCE_DIR}/common")
    set (VT_LIBS ${VT_LIBS} common)
endif(USE_VT)

option (USE_FFTW "Use FFTW for Fourier transform" ON)

if(USE_FFTW)
    include_directories("${client_SOURCE_DIR}/../../fftw++-1.13")
    add_subdirectory("${client_SOURCE_DIR}/../../fftw++-1.13" "${client_SOURCE_DIR}/FFTW")
    set (FFTW_LIBS ${FFTW_LIBS} fftw++)
endif(USE_FFTW)

option (USE_FF "Use FastFood implementation" ON)

if(USE_FF)
    include_directories("${client_SOURCE_DIR}/../../FastFood")
    add_subdirectory("${client_SOURCE_DIR}/../../FastFood" "${client_SOURCE_DIR}/FastFood")
    set (FF_LIBS ${FF_LIBS} FastFood)
endif(USE_FF)


add_executable(
${PROJECT_NAME} ${SRC_LIST}
${client_SOURCE_DIR}/add.h
${client_SOURCE_DIR}/experiment.h
${client_SOURCE_DIR}/lab.h
${client_SOURCE_DIR}/../../TensorSketch/TensorSketch.cpp
${client_SOURCE_DIR}/../../TensorSketch/CountSketch.cpp
${client_SOURCE_DIR}/../../common/Common.cpp
${client_SOURCE_DIR}/../../common/hadamardmatrix.cpp
${client_SOURCE_DIR}/../../common/normal_distribution.cpp
${client_SOURCE_DIR}/../../common/uniform_distribution.cpp
${client_SOURCE_DIR}/../../common/fastkernelexceptions.cpp
${client_SOURCE_DIR}/../../common/timer.cpp
${client_SOURCE_DIR}/../../common/dataconverter.cpp
${client_SOURCE_DIR}/../../common/datatransform.cpp
${client_SOURCE_DIR}/../../common/kernels.cpp
${client_SOURCE_DIR}/../../fftw++-1.13/fftw++.cc
${client_SOURCE_DIR}/../../FastFood/GaussSampler.cpp
${client_SOURCE_DIR}/../../FastFood/fastfood.cpp
${client_SOURCE_DIR}/../../FastFood/stacker.cpp
)
target_link_libraries(client fftw3 fftw3_omp gomp shark boost_serialization boost_system boost_filesystem boost_program_options)
