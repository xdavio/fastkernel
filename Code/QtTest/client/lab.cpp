#include "lab.h"

using namespace std;

Lab::Lab()
{
}

int Lab::AccuracyTest_FF_Gauss(unsigned i, unsigned d, unsigned D, double sigma, int n, unsigned int pad){
   ofstream outputfile;
   outputfile.open("../../../Report/Plots/Accuracy/Experiments/"+to_string(i)+"FastFood_"+to_string(d)+"_"+to_string(D)+".txt",ios::trunc);
   outputfile << "transform, d,D,RealKernel,FastKernel,difference" << endl;
   Experiment test;
   unsigned int testSamples = n;
   //test.useRandomData(testSamples,d);
   test.useCsvData(',',"dataset.txt","");
   FastFoodStacked stack(d+pad,D,sigma);
   double gamma = 1. / (2 * sigma * sigma);
   shark::GaussianRbfKernel<> RBF(gamma,false);

   outputfile << test.runAccuracyExperiment<FastFoodStacked>("FastFood",stack,RBF,pad);
   outputfile.close();
   return 0;
}

int Lab::AccuracyTest_TS_Poly(unsigned i, unsigned d, unsigned D,int p,double C,int n,double sparse){
   ofstream outputfile;
   outputfile.open("../../../Report/Plots/Accuracy/Experiments/"+to_string(i)+"TensorSketch_"+to_string(d)+"_"+to_string(D)+".txt",ios::trunc);
   outputfile << "transform, d,D,RealKernel,FastKernel,difference" << endl;
   Experiment test;
   unsigned int testSamples = n;
   //test.useRandomData(testSamples,d,sparse);
   test.useCsvData(',',"dataset.txt","");
   TensorSketch stack(d,D,p,C);
   shark::PolynomialKernel<> poly(p,C);
   outputfile << test.runAccuracyExperiment<TensorSketch>("TensorSketch",stack,poly);
   outputfile.close();
   return 0;
}

int Lab::SpeedTest(string filename, unsigned int d_,unsigned int n_){
    ofstream outputfile;
    outputfile.open("../../../Report/Plots/Speed/Experiments/"+filename+".txt",ios::trunc);
    outputfile << "transform, dimensions, features,n, repeats, avg, min(microseconds), max(microseconds)" << endl;
    Experiment test;
    unsigned int testSamples = n_;
    //vary features
    for(int D=d_;D<4097;D*=2){
        test.useRandomData(testSamples,d_);
        FastFoodStacked stack(d_,D,1);
        outputfile << test.runSpeedExperiment<FastFoodStacked>("FastFood features",stack,10);
        TensorSketch A(d_,D,2,0);
        outputfile << test.runSpeedExperiment<TensorSketch>("TS features",A,10);
        cout<<"finished feature: "<<to_string(D)<<endl;
        }
    //vary n
    for(int n=1;n<1000002;n+=100000){
        test.useRandomData(n,d_);
        FastFoodStacked stack(d_,d_,1);
        outputfile << test.runSpeedExperiment<FastFoodStacked>("FastFood n",stack,10);
        TensorSketch A(d_,d_,2,0);
        outputfile << test.runSpeedExperiment<TensorSketch>("TS n",A,10);
        cout<<"finished input count: "<<to_string(n)<<endl;
        }
    //vary input dimensions
    for(int d=d_;d<4097;d*=2){
        test.useRandomData(testSamples,d);
        FastFoodStacked stack(d,4096,1);
        outputfile << test.runSpeedExperiment<FastFoodStacked>("FastFood input d",stack,10);
        TensorSketch A(d,4096,2,0);
        outputfile << test.runSpeedExperiment<TensorSketch>("TS input d",A,10);
        cout<<"finished input dimension: "<<to_string(d)<<endl;
        }
    outputfile.close();
    return 0;
}

int Lab::InnerProdTest(){
    ofstream outputfile;
    outputfile.open("../../../Report/Plots/Accuracy/Experiments/innerproduct.txt",ios::trunc);
    outputfile << "transform, dimensions, features,real, estimate" << endl;
    Experiment test;
    test.useCsvData(',',"dataset.txt","");
    //vary features
    for(int D=16;D<4097;D*=2){
        for(int i =0;i<10;++i){
        test.useCsvData(',',"dataset.txt","");
        FastFoodStacked FF(16,D,2);
        shark::PolynomialKernel<> poly(2,0);
        outputfile << test.runInnerProductExperiment<FastFoodStacked>("FastFood IP",FF,poly,true);
        test.useCsvData(',',"dataset.txt","");
        TensorSketch TS(16,D,2,0);
        outputfile << test.runInnerProductExperiment<TensorSketch>("TS IP",TS,poly,false);
        }
        }
    outputfile.close();
    return 0;
}

int Lab::DifferenceTest(){
    ofstream outputfile;
    outputfile.open("../../../Report/Plots/Accuracy/Experiments/difference.txt",ios::trunc);
    outputfile << "transform, dimensions, features,real, estimate" << endl;
    Experiment test;
    test.useCsvData(',',"dataset.txt","");
    //vary features
    for(int D=16;D<4097;D*=2){
        for(int i =0;i<10;++i){
        test.useCsvData(',',"dataset.txt","");
        double gamma = 1. / (2*2*2);
        shark::GaussianRbfKernel<> RBF(gamma,false);
        FastFoodStacked FF(16,D,2);
        outputfile << test.runDifferenceExperiment<FastFoodStacked>("FastFood Diff",FF,RBF,true);
        test.useCsvData(',',"dataset.txt","");
        TensorSketch TS(16,D,1,0);
        outputfile << test.runDifferenceExperiment<TensorSketch>("TS Diff",TS,RBF,false);
        }
        }
    outputfile.close();
    return 0;
}
