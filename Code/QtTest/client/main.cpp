#include <iostream>
#include <Common.h>
#include "lab.h"
#include "experiment.h"
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector


using namespace std;
using namespace shark;




int FFACC(int argc, char* argv[])
{
    if(argc !=6){
        cout<<"please provide d,D,sigma,n for FF testing"<<endl;
        exit (EXIT_FAILURE);
    }
    cout<<"Experiment run. n"<<", avg. abs.error"<<", avg. rel. error"<<
          ", percentage rel.error<=1"<<endl;
    for(int i=0;i<4;++i){
        Lab MyLab;
        MyLab.AccuracyTest_FF_Gauss(i,strtoul(argv[2],NULL,10),strtoul(argv[3],NULL,10),strtod(argv[4],NULL),strtoul(argv[5],NULL,10));
    }
    return 0;
}

int FFACCPAD(int argc, char* argv[])
{
    if(argc !=7){
        cout<<"please provide d,D,sigma,n,pad for FF testing"<<endl;
        exit (EXIT_FAILURE);
    }
    cout<<"Experiment run. n"<<", avg. abs.error"<<", avg. rel. error"<<
          ", percentage rel.error<=1"<<endl;
    for(int i=0;i<4;++i){
        Lab MyLab;
        MyLab.AccuracyTest_FF_Gauss(i,strtoul(argv[2],NULL,10),strtoul(argv[3],NULL,10),strtod(argv[4],NULL),strtoul(argv[5],NULL,10),strtoul(argv[6],NULL,10));
    }
    return 0;
}


int TSACC(int argc, char* argv[]){
    if(argc !=8){
        cout<<"please provide d,D,p,C,n,sparse % for TS testing"<<endl;
        exit (EXIT_FAILURE);
    }
    cout<<"Experiment run. n"<<", avg. abs.error"<<", avg. rel. error"<<
          ", percentage rel.error<=1"<<endl;
    for(int i=0;i<4;++i){
        Lab MyLab;
        MyLab.AccuracyTest_TS_Poly(i,strtoul(argv[2],NULL,10),strtoul(argv[3],NULL,10),strtoul(argv[4],NULL,10),strtod(argv[5],NULL),strtoul(argv[6],NULL,10),strtod(argv[7],NULL));
    }
    return 0;
}

int TSACCsparse(int argc, char* argv[]){
    if(argc !=7){
        cout<<"please provide d,D,p,C,n for TS sparse testing"<<endl;
        exit (EXIT_FAILURE);
    }
    for(double i=0;i<=1;i+=0.1){
        Lab MyLab;
        MyLab.AccuracyTest_TS_Poly((unsigned int)(i*100),strtoul(argv[2],NULL,10),strtoul(argv[3],NULL,10),strtoul(argv[4],NULL,10),strtod(argv[5],NULL),strtoul(argv[6],NULL,10),i);
    }
    return 0;
}

int Speed(int argc, char* argv[]){
    if(argc !=4){
        cout<<"please provide d, n for speed testing"<<endl;
        exit (EXIT_FAILURE);
    }
    Lab MyLab;
    MyLab.SpeedTest("SpeedTest",strtoul(argv[2],NULL,10),strtoul(argv[3],NULL,10));
    return 0;
}

int InnerProd(int argc, char* argv[]){
    if(argc !=2){
        cout<<"No arguments needed for inner prod testing. Will try both methods using D=[16,4096]"<<endl;
        exit (EXIT_FAILURE);
    }
    Lab MyLab;
    cout<<"IP test"<<endl;
    cout<<"Experiment run. n"<<", avg. abs.error"<<", avg. rel. error"<<
          ", percentage rel.error<=1"<<endl;
    MyLab.InnerProdTest();
    cout<<"Diff test"<<endl;
    cout<<"Experiment run. n"<<", avg. abs.error"<<", avg. rel. error"<<
          ", percentage rel.error<=1"<<endl;
    MyLab.DifferenceTest();
    return 0;
}

int printDataset(int argc, char* argv[]){
    if(argc !=5){
        cout<<"please provide n,d, sparse % for random 0-1 data"<<endl;
        exit (EXIT_FAILURE);
    }
    unsigned int n = strtoul(argv[2],NULL,10);
    unsigned int d = strtoul(argv[3],NULL,10);
    double sparse = strtod(argv[4],NULL);
    Experiment exp;
    exp.useRandomData(n,d,sparse);
    exp.print_data();
    return 0;

}

int printPolyConstantDataset(int argc, char* argv[]){
    if(argc !=5){
        cout<<"please provide n,d, sparse % for fixed data"<<endl;
        exit (EXIT_FAILURE);
    }
    unsigned int n = strtoul(argv[2],NULL,10);
    unsigned int d = strtoul(argv[3],NULL,10);
    unsigned int sparse = strtoul(argv[4],NULL,10);

    for (int i=0;i<n;++i){
        vector<double> x;
        for (int i=0; i<d; ++i) x.push_back(9/std::sqrt(d));
        for (int i=0; i<sparse; ++i) x.push_back(0.0);
        //std::random_shuffle ( x.begin(), x.end() );
        common::print_vector<vector<double>>(x);
    }
    return 0;

}


int main(int argc,char* argv[]){
    if(argc>1){
        if(strcmp(argv[1],"FFACC")==0){
            return FFACC(argc,argv);
        }
        if(strcmp(argv[1],"FFACCPAD")==0){
            return FFACCPAD(argc,argv);
        }
        else if(strcmp(argv[1],"TSACC")==0){
            return TSACC(argc,argv);
        }
        else if(strcmp(argv[1],"TSACCsparse")==0){
            return TSACCsparse(argc,argv);
        }
        else if(strcmp(argv[1],"Speed")==0){
            return Speed(argc,argv);
        }

        else if(strcmp(argv[1],"DATARANDOM")==0){
            return printDataset(argc,argv);
        }
        else if(strcmp(argv[1],"DATAFIXED")==0){
            return printPolyConstantDataset(argc,argv);
        }
        else if(strcmp(argv[1],"IP")==0){
            return InnerProd(argc,argv);
        }
    }
    else{
        cout<<"Use with mode:"<<endl;
        cout<<"FFACC: Fast Food accuracy, provide d,D,sigma,n"<<endl;
        cout<<"FFACCPAD: Fast Food accuracy padding, provide d,D,sigma,n,pad"<<endl;
        cout<<"TSACC: Tensor Sketch accuracy, provide d,D,p,C,sparse %,n"<<endl;
        cout<<"TSACCsparse: Tensor Sketch accuracy over different sparse %. As above but do not provide sparse"<<endl;
        cout<<"Speed: Test the speed of both methods. Provide d,n"<<endl;
        cout<<"DATARANDOM: Print a random 0-1 dataset. Provide n,d, sparse"<<endl;
        cout<<"DATAFIXED: Print a fixed dataset with <x,y>=81. Provide n,d, sparse"<<endl;
    }
    return -1;

}
