#ifndef EXPERIMENT_H
#define EXPERIMENT_H
#include <shark/Data/Dataset.h>
#include <shark/Data/Csv.h>
#include <normal_distribution.h>
#include <uniform_distribution.h>
#include <datatransform.h>
#include <fastfood.h>
#include <timer.h>
#include <shark/Statistics/Statistics.h>
#include <kernels.h>
#include <Common.h>
#include <shark/Models/Kernels/GaussianRbfKernel.h>
#include <resizer.h>
#include <math.h>
#include <queue>

class Experiment
{
public:
    Experiment();
    void useRandomData(const int points, const int d_, const double sparse_=0);
    void print_data();
    void useCsvData(const char seperator,const std::string& dataFile,const std::string& labelFile = "");

    template <class T>
    string runSpeedExperiment(string experimentName,T& transformerClass,int repeat){
        common::Timer timer1;
        shark::Statistics stats;
        for(int i=-1;i<repeat;++i)
        {
            timer1.start();
            shark::Data<shark::RealVector>not_stored = shark::transform(m_dataset,transformerClass);
            timer1.stop();
            if(i>0){
                stats(timer1.microseconds());
            }
        }
        stringstream stream;
        string output;
        stream << experimentName << ", " << shark::dataDimension(m_dataset) << ", "<< transformerClass.features()<<", "<< m_dataset.numberOfElements()<<", " << repeat << ", " << stats(shark::Statistics::Mean()) << "," << stats(shark::Statistics::Min()) << "," << stats(shark::Statistics::Max()) << endl;
        output = stream.str();
        return output;

    }

    template <class T>
    string runAccuracyExperiment(string experimentName, T& transformerClass,shark::AbstractKernelFunction<shark::RealVector>& kernel, unsigned int pad=0){
        vector<double> fastkernel;
        vector<double> realkernel;

        shark::Data<shark::RealVector>dataCopy = m_dataset;
        dataCopy.makeIndependent();
        typedef shark::Data<shark::RealVector>::const_batch_reference BatchRef;

        if(pad!=0){
            resizer pow2(shark::dataDimension(m_dataset)+pad);
            m_dataset = shark::transform(m_dataset,pow2);
        }
        m_dataset = shark::transform(m_dataset,transformerClass);

        for(auto batch : dataCopy.batches()){
             for(std::size_t x = 0; x != boost::size(batch); ++x){
                 shark::RealVector vecX = shark::get(batch,x);
                 for(auto batchy : dataCopy.batches()){
                      for(std::size_t y = 0; y != boost::size(batchy); ++y){
                          shark::RealVector vecY = shark::get(batch,y);
                          realkernel.push_back(kernel.eval(vecX,vecY));
                           // if(kernel.eval(vecX,vecY)<0.2)
                             // cout<<std::to_string(x)<<", " <<std::to_string(y)<<"X: "<<common::toString_vector<shark::RealVector>(vecX)<< " ,Y: "<<common::toString_vector<shark::RealVector>(vecY)<<"Kernel: "<<benchmarkFunction(vecX,vecY)<<endl;
                      }
                 }
             }
        }

        for(auto batch : m_dataset.batches()){
             for(std::size_t x = 0; x != boost::size(batch); ++x){
                 shark::RealVector vecX = shark::get(batch,x);
                  for(auto batchy : m_dataset.batches()){
                      for(std::size_t y = 0; y != boost::size(batchy); ++y){
                          shark::RealVector vecY = shark::get(batch,y);
                          fastkernel.push_back(shark::blas::inner_prod(vecX,vecY));
                         // if(shark::blas::inner_prod(vecX,vecY)<-0.8)
                           // cout<<std::to_string(x)<<", " <<std::to_string(y)<<"X: "<<common::toString_vector<shark::RealVector>(vecX)<< " ,Y: "<<common::toString_vector<shark::RealVector>(vecY)<<"fast: "<<shark::blas::inner_prod(vecX,vecY)<<endl;
                      }
                 }
             }
        }

        stringstream stream;
        double absError = 0;
        double relError = 0;
        int relErrorCount =0;

        for(size_t t=0;t<fastkernel.size();++t){
            stream << experimentName << ", " <<shark::dataDimension(dataCopy)<< ", " <<shark::dataDimension(m_dataset)<< ", " << realkernel[t] << ", " << fastkernel[t] << ", " << realkernel[t]-fastkernel[t] <<endl;
            absError+=std::abs(realkernel[t]-fastkernel[t]);
            double relTurn = std::abs((realkernel[t])-(fastkernel[t]))/(realkernel[t]);
            if(relTurn<=1)
            {
            relError+=relTurn;
            relErrorCount +=1;
            }
        }
        //cout<<"Experiment run. n"<<", avg. abs.error"<<", avg. rel. error"<<
        //      ", percentage rel.error<=1"<<endl;
        cout<<fastkernel.size()<<","<<absError/fastkernel.size()<<","<<relError/relErrorCount<<
              ","<< (relErrorCount*1.0)/fastkernel.size()<<endl;


        string output;
        output = stream.str();
        return output;

    }
    template <class T>
    string runInnerProductExperiment(string experimentName, T& transformerClass,shark::AbstractKernelFunction<shark::RealVector>& kernel, bool FF, unsigned int pad=0){
        vector<double> fastkernel;
        vector<double> realkernel;
        queue<double> squaredNorms;

        shark::Data<shark::RealVector> transformed = shark::transform(m_dataset,transformerClass);

        for(auto batch : m_dataset.batches()){
             for(std::size_t x = 0; x != boost::size(batch); ++x){
                 shark::RealVector vecX = shark::get(batch,x);
                 for(auto batchy : m_dataset.batches()){
                      for(std::size_t y = 0; y != boost::size(batchy); ++y){
                          shark::RealVector vecY = shark::get(batch,y);
                          realkernel.push_back(kernel.eval(vecX,vecY));
                          squaredNorms.push(shark::blas::norm_sqr(vecX));
                          squaredNorms.push(shark::blas::norm_sqr(vecY));
                      }
                 }
             }
        }

        for(auto batch : transformed.batches()){
             for(std::size_t x = 0; x != boost::size(batch); ++x){
                 shark::RealVector vecX = shark::get(batch,x);
                  for(auto batchy : transformed.batches()){
                      for(std::size_t y = 0; y != boost::size(batchy); ++y){
                          shark::RealVector vecY = shark::get(batch,y);
                          double map = shark::blas::inner_prod(vecX,vecY);
                          if(FF){
//                              double x = squaredNorms.front();
//                              squaredNorms.pop();
//                              double y = squaredNorms.front();
//                              squaredNorms.pop();
//                              map= std::pow(4*std::log(std::abs(map))+x/2.0+y/2.0,2);

                              map= std::pow(4*std::log(std::abs(map))+shark::blas::norm_sqr(vecX)/2.0+shark::blas::norm_sqr(vecY)/2.0,2);

                          }
                          fastkernel.push_back(map);

                      }
                 }
             }
        }

        stringstream stream;
        double absError = 0;
        double relError = 0;
        int relErrorCount =0;

        for(size_t t=0;t<fastkernel.size();++t){
            stream << experimentName << ", " <<16<< ", " <<transformerClass.features()<< ", " << realkernel[t] << ", " << fastkernel[t] << ", " << realkernel[t]-fastkernel[t] <<endl;
            absError+=std::abs(realkernel[t]-fastkernel[t]);
            double relTurn = std::abs((realkernel[t])-(fastkernel[t]))/(realkernel[t]);
            if(relTurn<=1)
            {
            relError+=relTurn;
            relErrorCount +=1;
            }
        }
        cout<<fastkernel.size()<<","<<absError/fastkernel.size()<<","<<relError/relErrorCount<<
              ","<< (relErrorCount*1.0)/fastkernel.size()<<endl;


        string output;
        output = stream.str();
        return output;

    }

    template <class T>
    string runDifferenceExperiment(string experimentName, T& transformerClass,shark::AbstractKernelFunction<shark::RealVector>& kernel, bool FF, unsigned int pad=0){
        vector<double> fastkernel;
        vector<double> realkernel;

        shark::Data<shark::RealVector> transformed = shark::transform(m_dataset,transformerClass);

        for(auto batch : m_dataset.batches()){
             for(std::size_t x = 0; x != boost::size(batch); ++x){
                 shark::RealVector vecX = shark::get(batch,x);
                 for(auto batchy : m_dataset.batches()){
                      for(std::size_t y = 0; y != boost::size(batchy); ++y){
                          shark::RealVector vecY = shark::get(batch,y);
                          realkernel.push_back(kernel.eval(vecX,vecY));
                      }
                 }
             }
        }

        for(auto batch : transformed.batches()){
             for(std::size_t x = 0; x != boost::size(batch); ++x){
                 shark::RealVector vecX = shark::get(batch,x);
                  for(auto batchy : transformed.batches()){
                      for(std::size_t y = 0; y != boost::size(batchy); ++y){
                          shark::RealVector vecY = shark::get(batch,y);
                          double map = shark::blas::inner_prod(vecX,vecY);
                          if(!FF){
                              map= std::exp(-(-2*map +shark::blas::norm_sqr(vecX)+shark::blas::norm_sqr(vecY))/8.0);
                          }
                          fastkernel.push_back(map);

                      }
                 }
             }
        }

        stringstream stream;
        double absError = 0;
        double relError = 0;
        int relErrorCount =0;

        for(size_t t=0;t<fastkernel.size();++t){
            stream << experimentName << ", " <<16<< ", " <<transformerClass.features()<< ", " << realkernel[t] << ", " << fastkernel[t] << ", " << realkernel[t]-fastkernel[t] <<endl;
            absError+=std::abs(realkernel[t]-fastkernel[t]);
            double relTurn = std::abs((realkernel[t])-(fastkernel[t]))/(realkernel[t]);
            if(relTurn<=1)
            {
            relError+=relTurn;
            relErrorCount +=1;
            }
        }

        cout<<fastkernel.size()<<","<<absError/fastkernel.size()<<","<<relError/relErrorCount<<
              ","<< (relErrorCount*1.0)/fastkernel.size()<<endl;


        string output;
        output = stream.str();
        return output;

    }



    shark::Data<shark::RealVector> m_dataset;
private:
    shark::LabeledData<shark::RealVector,shark::RealVector> m_labeledData;

};

#endif // EXPERIMENT_H
