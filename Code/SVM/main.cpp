#include <iostream>
#include <shark/Algorithms/Trainers/McSvmOVATrainer.h> // the multiclass WW trainer
#include <shark/Algorithms/Trainers/CSvmTrainer.h>  //The CSvm trainer
#include <shark/Models/Kernels/GaussianRbfKernel.h> //the used kernel for the SVM
#include <shark/Models/Kernels/PolynomialKernel.h>
#include <shark/ObjectiveFunctions/Loss/ZeroOneLoss.h> //used for evaluation of the classifier
#include <shark/Data/DataDistribution.h> //includes small toy distributions
#include <shark/Unsupervised/RBM/Problems/MNIST.h>
#include <shark/Data/Libsvm.h>
#include <stacker.h>
#include <TensorSketch.h>
#include "resizer.h"
#include <timer.h>

using namespace std;
using namespace shark;

int toySample(){

    //generate data
    unsigned int ell = 500;     // number of training data point
    unsigned int tests = 10000; // number of test data points

    Chessboard problem; // artificial benchmark data
    ClassificationDataset training = problem.generateDataset(ell);
    ClassificationDataset test = problem.generateDataset(tests);

    //set kernel
    double gamma = 0.5;         // kernel bandwidth parameter
    GaussianRbfKernel<> kernel(gamma); // Gaussian kernel

    //The model is a kernel classifier
    KernelClassifier<RealVector> kc; // (affine) linear function in kernel-induced feature space

    //training:

    double C = 1000.0;          // regularization parameter
    bool bias = true;           // use bias/offset parameter

    CSvmTrainer<RealVector> trainer(&kernel, C, bias);
    cout << "Algorithm: " << trainer.name() << "\ntraining ..." << flush; // Shark algorithms know their names
    trainer.train(kc, training);
    cout << "\n  number of iterations: " << trainer.solutionProperties().iterations;
    cout << "\n  dual value: " << trainer.solutionProperties().value;
    cout << "\n  training time: " << trainer.solutionProperties().seconds << " seconds\ndone." << endl;

    //Evaluate the model
    ZeroOneLoss<unsigned int> loss; // 0-1 loss
    Data<unsigned int> output = kc(training.inputs()); // evaluate on training set
    double train_error = loss.eval(training.labels(), output);
    cout << "training error:\t" <<  train_error << endl;
    output = kc(test.inputs()); // evaluate on test set
    double test_error = loss.eval(test.labels(), output);
    cout << "test error:\t" << test_error << endl;
    return 0;

}

int linearFFToySample(int pad){
    unsigned int ell = 500;     // number of training data point
    unsigned int tests = 10000; // number of test data points

    Chessboard problem; // artificial benchmark data
    ClassificationDataset training = problem.generateDataset(ell);
    ClassificationDataset test = problem.generateDataset(tests);

    resizer pow2(2+pad);
    training = shark::transformInputs(training,pow2);
    test = shark::transformInputs(test,pow2);


    FastFoodStacked FF(2+pad,1024,1);
    training = shark::transformInputs(training,FF);
    test = shark::transformInputs(test,FF);

    //The model is a linear classifier
    LinearClassifier<RealVector> model;


    //training:

    double C = 1000.0;          // regularization parameter
    LinearCSvmTrainer<RealVector> trainer(C);
    cout << "Algorithm: " << trainer.name() << "\ntraining ..." << flush; // Shark algorithms know their names
    trainer.train(model, training);
    cout << "\n  number of iterations: " << trainer.solutionProperties().iterations;
    cout << "\n  dual value: " << trainer.solutionProperties().value;
    cout << "\n  training time: " << trainer.solutionProperties().seconds << " seconds\ndone." << endl;

    //Evaluate the model
    ZeroOneLoss<unsigned int> loss; // 0-1 loss
    Data<unsigned int> output = model(training.inputs()); // evaluate on training set
    double train_error = loss.eval(training.labels(), output);
    cout << "training error:\t" <<  train_error << endl;
    output = model(test.inputs()); // evaluate on test set
    double test_error = loss.eval(test.labels(), output);
    cout << "test error:\t" << test_error << endl;
    return 0;

}

int mnistGKernel(){
    ClassificationDataset training;
    import_libsvm(training,"mnist.scale");

    //speed it up a bit by dropping some of the training data
    //ClassificationDataset right = splitAtElement(training,20000);
    ClassificationDataset test;
    import_libsvm(test,"mnist.scale.t");

    //For some reason the testing set has one 778 features, so we add two blank ones to fit the 780 of the training set.
    resizer resize(780);
    test = shark::transformInputs(test,resize);


    //set kernel
    double gamma = 0.0073;         // kernel bandwidth parameter
    GaussianRbfKernel<> kernel(gamma); // Gaussian kernel

    //The model is a kernel classifier
    KernelClassifier<RealVector> kc; // (affine) linear function in kernel-induced feature space

    //training:

    double C = 100.0;          // regularization parameter
    bool offset = false;

    McSvmOVATrainer<RealVector> trainer(&kernel, C,offset);
    cout << "Algorithm: " << trainer.name() << "\ntraining ..." << flush; // Shark algorithms know their names
    trainer.train(kc, training);
    cout << "\n  number of iterations: " << trainer.solutionProperties().iterations;
    cout << "\n  dual value: " << trainer.solutionProperties().value;
    cout << "\n  training time: " << trainer.solutionProperties().seconds << " seconds\ndone." << endl;

    //Evaluate the model
    ZeroOneLoss<unsigned int> loss; // 0-1 loss
    Data<unsigned int> output = kc(training.inputs()); // evaluate on training set
    double train_error = loss.eval(training.labels(), output);
    cout << "training error:\t" <<  train_error << endl;
    output = kc(test.inputs()); // evaluate on test set
    double test_error = loss.eval(test.labels(), output);
    cout << "test error:\t" << test_error << endl;
    return 0;
}

int mnistPKernel(){
    ClassificationDataset training;
    import_libsvm(training,"mnist.scale");

    //speed it up a bit by dropping some of the training data
    //ClassificationDataset right = splitAtElement(training,20000);
    ClassificationDataset test;
    import_libsvm(test,"mnist.scale.t");

    //For some reason the testing set has one 778 features, so we add two blank ones to fit the 780 of the training set.
    resizer resize(780);
    test = shark::transformInputs(test,resize);


    //set kernel
    PolynomialKernel<> kernel(4,0);

    //The model is a kernel classifier
    KernelClassifier<RealVector> kc; // (affine) linear function in kernel-induced feature space

    //training:

    double C = 100.0;          // regularization parameter
    bool offset = false;

    McSvmOVATrainer<RealVector> trainer(&kernel, C,offset);
    cout << "Algorithm: " << trainer.name() << "\ntraining ..." << flush; // Shark algorithms know their names
    trainer.train(kc, training);
    cout << "\n  number of iterations: " << trainer.solutionProperties().iterations;
    cout << "\n  dual value: " << trainer.solutionProperties().value;
    cout << "\n  training time: " << trainer.solutionProperties().seconds << " seconds\ndone." << endl;

    //Evaluate the model
    ZeroOneLoss<unsigned int> loss; // 0-1 loss
    Data<unsigned int> output = kc(training.inputs()); // evaluate on training set
    double train_error = loss.eval(training.labels(), output);
    cout << "training error:\t" <<  train_error << endl;
    output = kc(test.inputs()); // evaluate on test set
    double test_error = loss.eval(test.labels(), output);
    cout << "test error:\t" << test_error << endl;
    return 0;
}

int mnistFFLinear(int flag,int samples,int D,double sigma){
    ClassificationDataset training;
    import_libsvm(training,"mnist.scale");
    if(samples!=60000){
    ClassificationDataset right = splitAtElement(training,samples);
    }
    ClassificationDataset test;
    import_libsvm(test,"mnist.scale.t");

    common::Timer t;
    t.start();

    resizer pow2(1024);
    training = shark::transformInputs(training,pow2);
    test = shark::transformInputs(test,pow2);
    FastFoodStacked FF(1024,D,sigma);
    training = shark::transformInputs(training,FF);
    test = shark::transformInputs(test,FF);

    t.stop();
    cout<<t.seconds()<<endl;

    cout<<"d: "<<inputDimension(training)<<endl
          <<"classes: "<<numberOfClasses(training)<<endl
         <<"n: "<<training.numberOfElements()<<endl;

    //Flag 0 = export data
    if(flag==0){
        shark::export_libsvm(training,"mnistFF"+to_string(D)+"train.dat",false,false,false);
        shark::export_libsvm(test,"mnistFF"+to_string(D)+"test.dat",false,false,false);
        return 0;
    }
    if(flag==1){

    //The model is a kernel classifier
    LinearClassifier<RealVector> kc;

    //training:

    double C = 100.0;          // regularization parameter

    LinearMcSvmOVATrainer<RealVector> trainer(C);
    cout << "Algorithm: " << trainer.name() << "\ntraining ..." << flush; // Shark algorithms know their names
    trainer.train(kc, training);
    cout << "\n  number of iterations: " << trainer.solutionProperties().iterations;
    cout << "\n  dual value: " << trainer.solutionProperties().value;
    cout << "\n  training time: " << trainer.solutionProperties().seconds << " seconds\ndone." << endl;

    //Evaluate the model
    ZeroOneLoss<unsigned int> loss; // 0-1 loss
    Data<unsigned int> output = kc(training.inputs()); // evaluate on training set
    double train_error = loss.eval(training.labels(), output);
    cout << "training error:\t" <<  train_error << endl;
    output = kc(test.inputs()); // evaluate on test set
    double test_error = loss.eval(test.labels(), output);
    cout << "test error:\t" << test_error << endl;
    return 0;
    }

    return -1;
}

int mnistTSLinear(int flag, int samples,int D, int p){
    ClassificationDataset training;
    import_libsvm(training,"mnist.scale");
    if(samples!=60000){
    ClassificationDataset right = splitAtElement(training,samples);
    }
    ClassificationDataset test;
    import_libsvm(test,"mnist.scale.t");
    resizer resize(780);

    common::Timer t;
    t.start();
    test = shark::transformInputs(test,resize);
    TensorSketch TS(780,D,2);
    training = shark::transformInputs(training,TS);
    test = shark::transformInputs(test,TS);

    t.stop();
    cout<<t.seconds()<<endl;

    cout<<"d: "<<inputDimension(training)<<endl
          <<"classes: "<<numberOfClasses(training)<<endl
         <<"n: "<<training.numberOfElements()<<endl;

    //Flag 0 = export data
    if(flag==0){
        shark::export_libsvm(training,"mnistTS"+to_string(D)+to_string(p)+"train.dat",false,false,false);
        shark::export_libsvm(test,"mnistTS"+to_string(D)+to_string(p)+"test.dat",false,false,false);
        return 0;
    }

    if(flag==1){

    //The model is a kernel classifier
    LinearClassifier<RealVector> kc;

    //training:

    double C = 100.0;          // regularization parameter

    LinearMcSvmOVATrainer<RealVector> trainer(C);
    cout << "Algorithm: " << trainer.name() << "\ntraining ..." << flush; // Shark algorithms know their names
    trainer.train(kc, training);
    cout << "\n  number of iterations: " << trainer.solutionProperties().iterations;
    cout << "\n  dual value: " << trainer.solutionProperties().value;
    cout << "\n  training time: " << trainer.solutionProperties().seconds << " seconds\ndone." << endl;

    //Evaluate the model
    ZeroOneLoss<unsigned int> loss; // 0-1 loss
    Data<unsigned int> output = kc(training.inputs()); // evaluate on training set
    double train_error = loss.eval(training.labels(), output);
    cout << "training error:\t" <<  train_error << endl;
    output = kc(test.inputs()); // evaluate on test set
    double test_error = loss.eval(test.labels(), output);
    cout << "test error:\t" << test_error << endl;
    return 0;
    }

    return -1;


}


int main(int argc,char* argv[])
{    
    //convert data
//    cout<<"I hope you gave me n, D,sigma,p"<<endl;
//    mnistFFLinear(0,strtoul(argv[1],NULL,10),strtoul(argv[2],NULL,10),strtod(argv[3],NULL));
//    mnistTSLinear(0,strtoul(argv[1],NULL,10),strtoul(argv[2],NULL,10),strtoul(argv[4],NULL,10));

    //linearFFToySample(strtoul(argv[1],NULL,10));

    //train
//    cout<<"Regular MNIST OVA G RFB:"<<endl;
//    mnistGKernel();
//    cout<<"Now using a linear classifier with fastfood (4096 features)."<<endl;
//    mnistFFLinear(1);
//    cout<<"Regular MNIST OVA Poly, p=2 C=0:"<<endl;
    mnistPKernel();
//    cout<<"Now using a linear classifier with TS (5000 features)."<<endl;
//    mnistTSLinear(1);
    return 0;
}

