#include <Common.h>
#include <complex>
#include <vector>
#include <algorithm>
#include <iostream>
#include <math.h>

using namespace std;
namespace common{

void print_FFTWdouble(const double* reals,const unsigned int n)
{
  vector<double> x(n);
  for(unsigned int i=0; i < n; i++) x[i]=reals[i];
  print_vector(x);
}

void print_FFTWcomplex(const complex<double>* complex,const unsigned int n)
{
  cout << '{' << complex[0];
  for(unsigned int i =1; i < n;i++)
      cout << ", " << complex[i];
    cout << '}' << endl;
}


vector <double> tensor_product(const vector<double>& a,const vector<double>& b)
{
  vector <double> x;
  for(unsigned int i=0;i<a.size();i++)
    {
      for(unsigned int j=0;j<b.size();j++)
	{
	  x.push_back(a[i]*b[j]);
	}
    }
  return x;
}



vector <double> normalize_vector(const vector<double>& vec)
{
  double length = 0;
  for(unsigned int i=0; i<vec.size();i++)
    {
      length+=pow(vec[i],2);
    }
  length=sqrt(length);
  
  vector <double> normalized;
  for(unsigned int i=0; i<vec.size();i++)
    {
      normalized.push_back(vec[i]/length);
    }
  
  return normalized;
}

//A power of two will have a binary number with just 1 non-zero entry. This is checked by bitwise AND (4= 100, 3=011, 4 & 3 ==0)
bool powerOfTwo(int d){
   return d>0 && ((d & (d-1)) == 0);
}


}
