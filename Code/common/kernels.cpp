#include "kernels.h"

namespace common{
namespace kernels{


double RBF(shark::RealVector& x, shark::RealVector& y){
    //check matching dimensions
    if(x.size()!=y.size())
        throw FastKernelExceptions::DimensionException();

    shark::RealVector x_min_y = x-y;
    double norm2 = shark::blas::norm_2(x_min_y);
    double norm2sq = std::pow(norm2,2);
    double norm2sqdiv = norm2sq/2;
    double result = std::exp(-norm2sqdiv);

    return result;
}

double Polynomial2(shark::RealVector& x, shark::RealVector& y){
    double result = std::pow(shark::blas::inner_prod(x,y),2);
    return result;
}
}
}
