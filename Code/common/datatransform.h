#ifndef DATATRANSFORM_H
#define DATATRANSFORM_H
#include <shark/LinAlg/Base.h>

namespace common{
class Datatransform
{
public:
    typedef shark::RealVector result_type;
    virtual shark::RealVector operator()(shark::RealVector input) const =0;
};
}

#endif // DATATRANSFORM_H
