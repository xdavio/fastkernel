#include "timer.h"
#include <exception>

using namespace std;
using namespace common;

Timer::Timer()
{
}

Timer::~Timer()
{
}

class TimerException1: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Timer ended before it was started.";
    }
}timer_exception_1;


void Timer::start(){
    m_is_running = 1;
    m_start = chrono::high_resolution_clock::now();
}

void Timer::reset(){
    start();
}

void Timer::stop(){
    if(!m_is_running) throw timer_exception_1;
    m_end = chrono::high_resolution_clock::now();
    m_is_running = 0;
}

long int Timer::hours(){
    if(m_is_running) {stop(); m_is_running=true;}
    return chrono::duration_cast<chrono::hours>(m_end-m_start).count();
}

long int Timer::minutes(){
    if(m_is_running) {stop(); m_is_running=true;}
    return chrono::duration_cast<chrono::minutes>(m_end-m_start).count();
}

long long int Timer::seconds(){
    if(m_is_running) {stop(); m_is_running=true;}
    return chrono::duration_cast<chrono::seconds>(m_end-m_start).count();
}

long long int Timer::milliseconds(){
    if(m_is_running) {stop(); m_is_running=true;}
    return chrono::duration_cast<chrono::milliseconds>(m_end-m_start).count();
}

long long int Timer::microseconds(){
    if(m_is_running) {stop(); m_is_running=true;}
    return chrono::duration_cast<chrono::microseconds>(m_end-m_start).count();
}

long long int Timer::nanoseconds(){
    if(m_is_running) {stop(); m_is_running=true;}
    return chrono::duration_cast<chrono::nanoseconds>(m_end-m_start).count();
}
