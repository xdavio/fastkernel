#include "GaussSampler.h"



GaussSampler::GaussSampler(const double  mean_, const double stdiv_) : mean(mean_), stdiv(stdiv_), mt(std::random_device()()), gauss_dist(mean_,stdiv_)
  {
  }

GaussSampler::~GaussSampler()
{
}

shark::RealMatrix GaussSampler::gaussMatrix(const unsigned int d, const unsigned int D)
{
  shark::RealMatrix A (D,d,0);
  for(unsigned int i=0;i<D;++i){
      for(unsigned int j=0;j<d;++j){
          A(i,j)=sample();
      }
  }
  return A;
}

std::vector<double> GaussSampler::gaussSquareDiagonal_abs(const unsigned int d){
    std::vector<double> G;
    for(unsigned int i=0;i<d;++i){
        G.push_back(d*std::abs(sample()));
    }
    return G;
}

std::vector<double> GaussSampler::gaussSquareDiagonal(const unsigned int d){
    std::vector<double> G;
    for(unsigned int i=0;i<d;++i){
        G.push_back(sample());
    }
    return G;
}

double GaussSampler::sample()
{
  return gauss_dist(mt);
}




