# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

with open('Experiments/1FastFood_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel = [float(x[3]) for x in data if x[0]== 'FastFood' ]
fast = [float(x[4]) for x in data if x[0]== 'FastFood' ]

fig, ax = plt.subplots()

plt.scatter(kernel,fast,s=2,alpha=0.1)

ax.set_ylim(0, 1)
ax.set_xlim(0, 1)

plt.title('FastFood estimate juxtaposed with Gaussian RBF')
plt.xlabel('Kernel Value')
plt.ylabel('FastFood estimate')

plt.savefig('FF_Scatter_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.png', bbox_inches=0,dpi=300)

