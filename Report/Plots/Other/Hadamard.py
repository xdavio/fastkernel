# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('verifyhadamardresults.txt', 'Ur') as f:
    data = list(tuple(rec) for rec in csv.reader(f, delimiter=','))

axislabels = [float(x[0]) for x in data]
slow = [float(x[2]) for x in data]
fast = [float(x[3]) for x in data]


fig, ax = plt.subplots()
ax.plot(axislabels, slow, 'b^', label='Matrix multiplication')
ax.plot(axislabels, fast, 'ro', label='Fast-Walsh Hadamard transform')
ax.plot(axislabels, slow, 'k--', label='_nolegend_')
ax.plot(axislabels, fast, 'k--', label='_nolegend_')

ax.set_xlim(2, 1024)
ax.set_xticks([2,64,128,256,512,1024])

#ax.set_yscale('log',basey=2)

plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
plt.legend(numpoints=1,loc=2)
plt.title('FWHT vs. Matrix multiplication transform')
plt.xlabel('Input dimensions')
plt.ylabel('Microseconds')

plt.savefig('hadaplot.png', bbox_inches=0)

