%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 

Fastfood and tensor sketching both promise to deliver fast and accurate kernel estimates\cite{Le2013,Pham2013}. To test the speed and performance of the methods we conducted a series of experiments on the implementation introduced in the previous chapter. We first investigate the accuracy of each method and try to pinpoint the conditions that give the most accurate kernel approximation. In section.\ref{section:speed} we look at the speed of each method and how the theoretical expectations compare to our observations. Finally we put each method to use on the MNIST classification task to see how the combination of kernel approximation and linear classification compare to using the kernel trick and non-linear support vector machines.

\section{Kernel approximation}

Previous work has shown that the methods can work\cite{Pham2013, Le2013}, but do not provide much insight in the technical challenges involved in implementing and optimizing these methods. These experiments have two purposes. To verify the theoretic expectations on accuracy and speed independent of the original implementations and to extract new information on the kernel and input data properties involved in attaining the best results.

To recap, the promise of the mappings are that:

\begin{equation}
  \label{eq:kernelPromise}
  E[\langle \bm{z}(x),\bm{z}(y)]=k(x,y)
\end{equation}

So to test this promise experimentally on a dataset of $n$ entries we can use these error functions:

\begin{equation}
  \label{eq:absmeasure}
  Error_{abs.}=\frac{1}{n}\sum_{x,y}^n|\langle \bm{z}(x),\bm{z}(y)\rangle-k(x,y)|
\end{equation}

\begin{equation}
  \label{eq:relmeasure}
  Error_{rel.}=\frac{1}{n}\sum_{x,y}^n\frac{|\langle \bm{z}(x),\bm{z}(y)\rangle-k(x,y)|}{k(x,y)}
\end{equation}

The relative error function is troublesome to work with here because some kernels are very close to zero, a small error in the estimation of these values can skew the average relative error severely. The absolute value on the other hand is hard to compare across different kernels and parameters. To get an easily comparable and readable error measure we measured the relative error as above, but only include pairs where the relative error was $\leq100\%$. If the percentage of inputs fulfilling this condition is less than $100\%$ it is reported along with $Error_{rel}$ as $E_\%$.


Figure \ref{fig:FF165123} show kernel estimates from the Fastfood method plotted against exact kernel values. Each point represents a combination of two vectors $x,y$. The coordinate corresponds to $(k(x,y),\langle\bm{z}(x),\bm{z}(y)\rangle)$. A perfect mapping would manifest as a narrow 45-degree line. Previous results analyze the precision on an aggregated level\cite{Pham2013}. Using this plot we can more easily get a nuanced understanding of the methods ability to estimate the kernel value. 


 \begin{figure}[h]
  \centering
  \includegraphics[width=0.9\textwidth]{Accuracy/FF_Scatter_16_512_15}  
  \caption{Accuracy test of Fastfood.$d=16,D=512,\sigma=1.5,Error_{Rel.}=3.5\%$}
  \label{fig:FF165123}
\end{figure}

Figure \ref{fig:FF165123} shows Fastfood estimates for a dataset of 100 vectors sampled uniformly from $[0,1]^{16}$ using 512 features.

\subsection{Tensor Sketch input normalization}
Using randomly generated data sampled from $[-1,1]$ we experienced much lower accuracy for Tensor Sketching than expected. Vectors sampled uniformly i.i.d from $[-1,1]^d$ have an expected inner product of zero. This means that we get a high relative error, and it also makes it difficult to investigate a larger spectrum of the kernel range because values concentrate on zero. With no cost to generality we can transform data into a positive range before sketching. This gives a much broader range of  values and should make classification easier. It also demonstrates that the polynomial kernel is not shift invariant. Looking at figure \ref{fig:-11vs01} we see the difference in the range and accuracy of estimation kernel. 

\begin{figure}[h]%
    \centering
    \subfloat[$\bm{x}\in \lbrack-1,1\rbrack^d,Error_{rel.}=42\%,E_\%=39\%$]{{\includegraphics[width=0.5\textwidth]{Accuracy/TS_Scatter_16_512_-11}}}%
    \subfloat[$\bm{x}\in \lbrack0,1\rbrack^d,Error_{rel.}=13\%$]{{\includegraphics[width=0.5\textwidth]{Accuracy/TS_Scatter_16_512_01}}}%
    \caption{\textbf{Tensor sketching random data}\\$d=16,D=128,p=2,C=0$}%
    \label{fig:-11vs01}%
\end{figure}

% \begin{figure}[h]
%  \centering
%  \includegraphics[width=0.9\textwidth]{Accuracy/-11vs01}  
%  \caption{Tensor sketching random data.Left: $\bm{x}\in [-1,1]^d,Error_{rel.}=42\%,E_\%=39\%$. Right: $\bm{x}\in [0,1]^d,Error_{rel.}=13\%$.Both $d=16,D=128,p=2,C=0$}
%  \label{fig:-11vs01}
%\end{figure}


Unless anything else is stated explicitly the experiments described here were conducted with data sampled i.i.d. from a uniform distribution between $[0,1]$.

\subsection{Increasing the feature count}\label{experiement:increasingD}

The first experiment concerns the feature count, $D$. Looking at the error bounds given for Fastfood,p.\pageref{FF:errorBound}, and tensor sketching,eq.\ref{eq:TensorSketchVar}, we would expect the approximation error to fall sharply with increasing feature count.

 \begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{Accuracy/FFTSerrorD}  
  \caption{Error decreases as a function of feature count,D.$d=16,\sigma=2,p=2$}
  \label{fig:FFTSerror}\
\end{figure}

The results in fig.\ref{fig:FFTSerror},\ref{fig:FFerror} and
table.\ref{tab:FFTSerror} are averages over $16$ runs of each
algorithm. Each run on a randomly generated dataset of $10000$ vector
pairs from vectors $\in[0,1]^{16}$. The relative error depends heavily
on the choice of kernel parameters. By choosing dominating $C$ and
$\sigma$ that effectively place a strict limit on the range of the
kernel we can artificially lower the error. We will discuss this
further in the next experiment. For this experiment we use $C=0$ and
$\sigma=2$ as these parameters give a decent spread over the kernel
range. 

\begin{table}[H]
\caption{\textbf{Kernel estimation error} $d=16,\sigma=2,p=2,C=0$}
\label{tab:FFTSerror}
\centering
\begin{tabular}{l|cc|cc}
\textbf{$D$}   & \textbf{$Fastfood_{Abs.}$}& \textbf{$Fastfood_{Rel.}\%$}& \textbf{$Tensor Sketch_{Abs.}$}& \textbf{$Tensor Sketch_{Rel.}\%$} \\
\hline
16  & 0.083  & $11.82$ & 8.39 & $44.01$  \\
32  & 0.057  & $8.14$ & 5.56 & $34.32$ \\
64  & 0.047 & $6.74$  & 4.99 & $28.57$  \\
128 & 0.033 & $4.68$  & 3.63 & $22.02$ \\
256 & 0.022 & $3.08$  & 2.19 & $12.98$ \\
512 & 0.015 & $2.15$  & 1.32 & $7.83$ \\
1024& 0.011 & $1.56$ & 0.81 & $4.95$\\
2048& 0.0077 & $1.08$  & 0.39 & $2.74$ \\
4096& 0.0052 & $0.73$ & 0.32 & $2.04$\\
8192& 0.0036 & $0.51$ & 0.31 & $1.73$ \\         
\end{tabular}
\end{table}

The performance seems on par with the results presented in the
original papers\cite{Le2013,Pham2013}. This is particularly
interesting for Fastfood since our implementation uses a novel way of
sampling the scaling matrix.

Tensor sketching does show a more fluctuating performance than
Fastfood. As an example, for $D=4096$ the best tensor sketch mapping
had an accuracy of $1\%$ but the worst $4.1\%$. This is likely due to
the way we choose $h$ functions. They are mappings sampled uniformly
i.i.d. to map from $[d]$ to $[D]$. While this gives a good performance
expectancy, it also allows cases where the performance is off. In the
worst case the whole input is hashed to the same position in the
sketch. Future work could study how to best construct these hash
functions. It seems that for many applications collisions could be
completely avoided since we can choose $D\gg d$. This could improve
the accuracy of tensor sketching beyond the results shown here.



 \begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{Accuracy/FF_Scatter_progress}  
  \caption{Fastfood estimates improve with D.$d=16,\sigma=3$}
  \label{fig:FFerror}\
\end{figure}

\subsection{Gaussian RBF Kernel range}

The estimates are not equally good across the range of the kernel. As we mentioned briefly in the previous experiment kernel parameters greatly effect accuracy of the estimates. 

 \begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{Accuracy/sigma15vs2}  
  \caption{Changing the kernel range. Left:$sigma=1.5,Error_{rel.}=8.3\%$, right $\sigma=2,Error_{rel.}=3.6\%$. Both $d=16,D=128$.}
  \label{fig:FFerrorRange}\
\end{figure}


Fig.\ref{fig:FFerrorRange} gives an impression of how the approximation suffers for lower kernel values. There is an interesting tradeoff between kernel resolution and the ability to accuratly estimate the kernel. Figure \ref{fig:FFerrorDistribution} shows how the average relative error, in discreet sections of the kernel range, drops for higher values. This highlights the importance of choosing the right $\sigma$ value. By choosing sigma to fit data in the high part of the Gaussian RBF range estimates are expected to be more similar to using the kernel, but with a downside of less resolution for a classifier to work with.

 \begin{figure}[t]
  \centering
  \includegraphics[width=0.9\textwidth]{Accuracy/FFboxplot}  
  \caption{Error over kernel range. $d=8,D=128,sigma=0.6,Error_{Abs.}=0.059$. Shows the distribution in absolute error }
  \label{fig:FFerrorDistribution}\
\end{figure}

This behavior is likely due to the way vector entries are summed in the Fastfood map. The $||\bm{x}-\bm{y}||$ of the kernel captures the difference between inputs on each dimension, the $\langle\bm{v_j},\bm{x}\rangle$ of the mapping captures the difference on an aggregated level. Consider in the extreme case two orthogonal unit vectors. The kernel will easily identify these as different, but they have the same expected value in the Fastfood mapping.  The $B$ and $S$ matrices of Fastfood ensure that they will not likely have the exact same value, but this effect makes it hard for Fastfood to maintain differences based on variations across the input dimensions. Vector pairs that are dissimilar across all dimensions and similar vectors are less distorted by the aggregation. Increasing $\sigma$ improves the error because it lessens the kernels discrimination between dissimilar pairs, not because it improves the accuracy of Fastfood.

\newpage 
\subsection{Mappings for Fastfood}\label{head:FFmappings}


Testing the mappings presented for Random Kitchen Sinks in chapter \ref{section:RKS} we found that the $\bm{z''}$ mapping was faster, as expected, but also represented a step down in terms of accuracy. Hence the implementation and experiments presented as part of this thesis uses the $\bm{z}$ mapping as defined in eq.\ref{eq:normal_map}:

\begin{equation}
z_j(x)=\frac{1}{\sqrt{D}}\{\cos([\Omega\bm{x}]_j),\sin([\Omega\bm{x}]_j)\}
\end{equation}

Results for the $\bm{z''}$ mapping can be found in appendix \ref{2cosappendix}.

\subsection{Sparse input for Tensor Sketch}

The main loss of information suffered in the Tensor Sketch approach is from collisions in the Count Sketching step. In illustration \ref{ill:CountSketch} e.g. we see a collision occurring on the second and last entry in the sketch. Having sparse vectors should reduce the loss of information suffered in such collisions because the chance that one of the colliding inputs is zero increases. At the same time all non-zero entries are still appearing in the sketch. To verify this experimentally we conducted a series of experiments with differing input sparsity. 


 \begin{figure}[h]
  \centering
  \includegraphics[width=0.9\textwidth]{Accuracy/TSsparseError2}  
  \caption{Relative error over input sparsity. $d=40,D=128,p=2,n=100$. Average over 12 runs. }
  \label{fig:TSsparseError}\
\end{figure}

When sparsity goes up in our random dataset, the polynomial kernel values generally drop, making it hard to isolate the effect of sparsity alone. To measure it we need to scale the non-zero entries up as sparsity increases. We construct data vectors $\bm{x}$ with entries  $x_d=\frac{\sqrt{k}}{d-s}$ where $s$ is the number of sparse entries. We must also make sure that the zero-entries remain in a fixed part of the constructed input, having random sparsity would again make the actual kernel smaller and thus increase relative error. With this rather artificial input construction kernel value remains constant. Notice that this construction of the input data is only necessary for measuring the isolated effect of sparsity on kernel approximation.

Looking at fig.\ref{fig:TSsparseError} we see that the effects of increasing sparsity are complicated. While the error generally does drop as sparsity increases, it also rises especially in the beginning. This might be due to the fact that scaling the entries up also mean that each collision that does occur causes greater distortion.  

\subsection{Accuracy comparison}
 We can use eq.\ref{eq:gaussPoly} to make a direct comparison between Fastfood and tensor sketching. The Fastfood method works on shift-invariant kernels and the length of the original input is lost when mapping. Tensor sketching on the other hand maintains a good estimate of the original input length. Fig.\ref{fig:convert} shows this difference when we use eq.\ref{eq:gaussPoly} to directly transform the polynomial kernel estimates into estimates for the Gaussian RBF and vice versa.

\begin{figure}[h]%
    \centering
    \subfloat[Fastfood mapping converted]{{\includegraphics[width=0.5\textwidth]{Accuracy/FFTSipError}}}%
    \subfloat[Tensor sketch mapping converted]{{\includegraphics[width=0.5\textwidth]{Accuracy/FFTSDiffError}}}%
    \caption{\textbf{Converting estimates}\\$d=16,p=2,\sigma=2$}%
    \label{fig:convert}%
\end{figure}


This does not provide a way to use tensor sketching as a map for RBF kernels, but it gives some impression of the amount of information loss. It is clear that Fastfood maintains much less information after mapping than tensor sketching. The converted estimates are very inaccurate for Fastfood and very precise for tensor sketch. If we use information on the lengths from the original input Fastfood does provide an accurate conversion, see fig.\ref{fig:FFcheatconversion} in the appendix. This shows how Fastfood captures only the directions of vectors while tensor sketching also conserves the length. Pham and Pagh presents the idea of applying tensor sketching for the Gaussian kernel through Taylor-series approximations\cite{Pham2013}. This experiment suggests that such methods might provide more accurate mappings for the Gaussian RBF than Fastfood.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 
