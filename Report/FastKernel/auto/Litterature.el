(TeX-add-style-hook "Litterature"
 (lambda ()
    (LaTeX-add-bibitems
     "Bochner"
     "CountSketch"
     "TensorSketch"
     "CountMinSketch"
     "Smola09"
     "RR07"
     "Pagh11")))

