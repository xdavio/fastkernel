\section{The Kernel Trick}
We begin by introducing the Kernel Trick along with some general machine learning concepts following the terminology of Bishop\cite{Bishop2006}. This will provide a basis of discussion throughout the rest of the thesis and demonstrate the usefulness of the random projections presented later.  

In many machine learning scenarios we wish to find the parameters of a decision 
function $f(\bm{x})$ of the form:
\begin{equation}
f(\bm{x}) = w_0+w_1\phi_1(\bm{x})+ \cdots + w_d\phi_d(\bm{x}) = \bm{w}^T\bm{\phi(x)}
\label{eq: Linear Model}
\end{equation}

Such a function is called a linear model, since it represents a linear combination of
fixed nonlinear functions $\phi_j$, or \textsl{features}, of the input variables \cite{Bishop2006}. Evaluating this function has uses in regression and classification. Learning the parameters of such a function, $\bm{\omega}$, is a common machine learning task and many popular algorithms like \textsl{Support Vector Machines} or \textsl{Adaboost} refer to various ways of solving this optimization task\cite{Rahimi2008}. Evaluating the decision function with known parameters is referred to as \textsl{testing} while learning the parameters is \textsl{training} and generally requires at least $O(n)$ evaluations of the decision function for a data set with $n$ entries.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{KernelTrick} 
\caption{Mapping to a feature space where the classes are linearly separable}
\label{ill:KernelTrick}
\end{figure}


By using a feature space $\bm{\phi}$, data that originally had a very complex decision boundary can be mapped to a space where a simple linear separation of the classes exists. The idea is illustrated in fig.\ref{ill:KernelTrick}. Unfortunately the feature space can often be of very high or even infinite dimensionality, making a direct computation of the mapping undesirable and risking incurring effects of the curse of dimensionality.

However, many learning algorithms do not require actually mapping the input to the feature space $\bm{\phi}$, but just some notion of the 'difference' between datapoints in this space. They can be reformulated in a \textsl{dual representation}:
\begin{equation}
f(\bm{x}) = \bm{w}^T\bm{\phi(x)} = \sum_{n=1}^N\alpha_n\left\langle \bm{\phi}(\bm{x_n}),\bm{\phi}(\bm{x})\right\rangle
\label{eq:Kernel expansion}
\end{equation}
The dual representation relies only on inner products so the feature space could be any Hilbert space. Therefore it is desireable to find functions $k(\bm{x},\bm{y})$ with the property that:  

\begin{equation}
k(\bm{x},\bm{x}')=\bm{\phi}(\bm{x})^T\bm{\phi}(\bm{x}')
\label{eq:Kernel trick}
\end{equation}

The big benefit of using this formulation is that the problem becomes
expressed only in terms of $k(\bm{x},\bm{x'}$) and we no longer have
to work with $\bm{\phi}$. Such functions are called \emph{kernel
  functions} or just kernels. Kernel Methods were first introduced in
the field of pattern recognition in
1964\cite{M.AizermanE.Braverman1964} but have flourished with their
applications in Support Vector Machines introduced in
1995\cite{Cortes1995}. Their properties are described by Mercers
Theorem\cite{Mercer1909} which guarantees the expansion of kernel
functions to Hilbert spaces as in eq.\ref{eq:Kernel trick}, requiring
the kernels to be positive, continuous and semi-definite. The
representer Theorem\cite{Wahba1970} states that the expansion exists
under manageable conditions, even when the feature space is of
infinite dimensionality. See appendix \ref{app:Mercer} for further
details. Using kernel functions we can evaluate the decision function
as:
\begin{equation}
\label{eq:decision function kernel}
f(\bm{x})= \sum_{n=1}^N\alpha_nk(\bm{x}_n,\bm{x})
\end{equation}

Using a dual representation a Support Vector Machine, originally a linear classifier, can discover non-linear relationships. The \emph{kernel trick} refers to the idea of replacing expensive mappings to high dimensional feature spaces, $\bm{\phi}$, with computationally inexpensive evaluations of a kernel function $k(\bm{x},\bm{x}')$ and learning through the dual representation of a problem.



\subsection{Common Kernel functions}
The choice of kernel function is a matter of the intended application
and the data. It is desirable to choose a kernel function that
corresponds to a mapping $\bm{\phi}(\bm{x})$ into some feature space where the
input is easily separable. Two of the most commonly used types of
kernel functions are \emph{Radial Basis Function kernels} and
\emph{polynomial kernels}.

\begin{definition}[RBF kernels]
Kernels depending only on the magnitude of the distance between inputs, $k(\bm{x},\bm{x'})=k(||\bm{x}-\bm{x'}||)$, e.g. the Gaussian kernel:
  \begin{equation}
   k(\bm{x},\bm{y})=\exp(-\frac{||\bm{x}-\bm{y}||^2}{2\sigma^2})
  \end{equation}
  Where $\sigma$ is a free parameter. The corresponding feature space is of infinite dimensionality\cite{Bishop2006}.
\label{def:RBF}
\end{definition}

\begin{definition}[Polynomial Kernels]
Polynomials of the inner product of the input.
\begin{equation}
  k(\bm{x},\bm{y})=(\langle \bm{x},\bm{y}\rangle+c)^p
  \end{equation}
  Where $c$ is a constant. If $c=0$, we call the kernel homogeneous. The corresponding mapping consists of all $p$-degree terms of the input. We will see how this fact becomes useful in the tensor sketching approach.
\label{def:PolyKernel}
\end{definition}

\begin{table}[h]
\centering
\caption{Common definitions}
\begin{tabular}{|c|l|}
\hline
$n$ & Size of a dataset. The number of entries. \\
$d$ & Dimensionality of input.\\
$D$ & Dimensionality of feature space or mapping.\\
$p$ & Degree of polynomial kernel.\\
$C$ & Offset of polynomial kernel.\\
$\sigma$ & Width of Gaussian RBF kernel.\\
\hline
\end{tabular}
\end{table}
\subsubsection{Kernel Conversion}\label{head:conversion}

The Gaussian RBF and the polynomial kernel are related through the property of the inner product:

\begin{equation}
  \label{eq:gaussPoly}
  ||\bm{x}-\bm{y}||^2=||\bm{x}||^2+||\bm{y}||^2-2\langle \bm{x},\bm{y}\rangle
\end{equation}

So knowing the length of the involved vectors we can establish a linear time conversion between an RBF kernel and a polynomial kernel.
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 
