\newpage
\section{Tensor Sketching}

The second technique we will explore is based on tensor sketching. Tensor sketching in turn relies on fast convolution of Count Sketches\cite{Pagh2012} to approximate polynomial kernels\cite{Pham2013}. We first look at using the tensor product directly as a mapping for the polynomial kernel. We then introduce a version of the Count Sketch algorithm. Finally we show how Count Sketch can be used make fast and accurate sketches of the tensor product using the fast convolution method of Pagh\cite{Pagh2012}. Such sketches fulfill the purpose of approximating kernel values, but here for the polynomial kernel. End the end of the section we will have a mapping $\bm{z}$ such that: 

\begin{equation}
E[\langle\bm{z}(\bm{x}),\bm{z}(\bm{x'})\rangle]=(\langle\bm{x},\bm{x'}\rangle+c)^p
\end{equation}

\subsection{The tensor product and the polynomial kernel }

\begin{definition}[Tensor Product]
Given a vector $\bm{x}\in\mathbb{R}^d$ its 2-level tensor product is defined as:
\begin{equation}
\bm{x}^{(2)}=\bm{x}\otimes \bm{x} =
\begin{bmatrix}
x_1x_1 & x_1x_2 & \cdots & x_1x_d \\
x_2x_1 & x_2x_2 & \cdots & x_2x_d \\
\vdots & \vdots & \ddots & \vdots \\
x_dx_1 & x_dx_2 & \cdots & x_dx_d \\
\end{bmatrix}
\in\mathbb{R}^{d^2}
\end{equation} 
Here written as a matrix, but we normally interpret the tensor product as a vector. The general p-level tensor product:
\begin{equation}
x^{(p)}=x \otimes ... \otimes x \text{, p - 1 times}
\end{equation}
is a mapping $\mathbb{R}^d\rightarrow\mathbb{R}^{d^p}$. It is similar to a Cartesian product, but ordered and with multiplication between the entries in each tuple. We introduce a general notation along those lines: 

\begin{equation}
x^{(p)}=\bigotimes_{i_1,..,i_p=1}^{d}\prod_{j\in\{i_1,\cdots,i_p\}}x_j
\end{equation}
\end{definition}

The big $\otimes$ is understood to provide $p$ indices between $1$ and $d$, the multiplication part provides the product of the corresponding entries of $\bm{x}$. The resulting vector will be $x_1^p,x_1^{p-1}x_2,\dots,x_d^p$. In this way the tensor product contains all possible $p$-degree ordered multiples of the elements of $\bm{x}$. This makes it an explict mapping for the p-degree polynomial kernel as stated in definition \ref{def:PolyKernel}.

\begin{lem}
\label{lem:TensorMap}
Given two vectors $\bm{x},\bm{y}\in\mathbb{R}^d$ the inner product of their $p$-level tensor products is exactly equal to the value returned by the homogenoeus polynomial kernel:
\begin{equation}
\langle \bm{x}^{(p)},\bm{y}^{(p)}\rangle = \langle \bm{x},\bm{y}\rangle^p 
\end{equation}
\end{lem}

\begin{proof}
Given two vectors $\bm{x}={x_1,x_2,...,x_d}$ and $\bm{y}={y_1,y_2,...,y_d}$. Their p-level tensor products are :

\begin{centering}
\begin{equation}
\bm{x}^{(p)}=\bigotimes_{i_1,..,i_p=1}^{d}\prod_{j\in\{i_k,\cdots,i_p\}}x_j
\end{equation}
and
\begin{equation}
\bm{y}^{(p)}=\bigotimes_{i_1,..,i_p=1}^{d}\prod_{j\in\{i_k,\cdots,i_p\}}y_j
\end{equation}
\end{centering}

We can directly write the inner product between them as:
\begin{equation}
\langle \bm{x}^{(p)},\bm{y}^{(p)}\rangle = \sum_{i_1,\dots,i_p=1}^{d,\cdots,d}(\prod_{j\in\{i_1,\cdots,i_p\}}x_j)*(\prod_{j\in\{i_1,\cdots,i_p\}}y_j)
\end{equation}
\begin{equation}
\langle \bm{x}^{(p)},\bm{y}^{(p)}\rangle = \sum_{i_1,\dots,i_p=1}^{d,\cdots,d}(\prod_{j\in\{i_1,\cdots,i_p\}}x_jy_j)
\end{equation}

Change the order of evaluation.
\begin{equation}
\langle \bm{x}^{(p)},\bm{y}^{(p)}\rangle =\prod^p \sum_{i=1}^dx_{i}y_{i} \nonumber
\end{equation}
\begin{equation}
=(\sum_{i=1}^dx_{i}y_{i})^p=\langle \bm{x},\bm{y} \rangle ^p
\end{equation}
\end{proof}

\begin{lem}
\label{lem:TensorConstant}
By appending a constant $\sqrt{c}$ to a vector $\bm{x}\in\mathbb{R}^d$ the tensor sketch of $\bm{x}$ can provide an explict mapping for any polynomial kernel.
\begin{equation}
\langle \bm{x}^{(p)},\bm{y}^{(p)}\rangle = (\langle \bm{x},\bm{y}\rangle+c)^p 
\end{equation}
\end{lem}
\begin{proof}
appending $\sqrt{c}$ to the end of $x$ and $y$ in the previous proof we arrive at the statement:
\begin{equation}
=(\sum_{i}^{d+1}x_{i}y_{i})^p=(\sum_{i}^{d}x_{i}y_{i}+\sqrt(c)*\sqrt(c))^p=(\langle \bm{x},\bm{y} \rangle + c)^p
\end{equation}
\end{proof}

So the tensor product provides a mapping $\bm{z}(\bm{x})$ allowing us to work directly in a feature space corresponding to any polynomial kernel. Since the tensor product belongs to $\mathbb{R}^{d^p}$ it is not a scalable approach however. Calculating the mapping directly takes $O(Nd^p)$ for a dataset of $N$ points in $\mathbb{R}^d$. For cases with low dimensional data and using a polynomial kernel of low degree, the tensor product approach scales well in the number of points. However if $d$ or $p$ are large a direct tensor product mapping quickly becomes computationally more expensive than using the kernel trick and suffering the curse of support. To improve on this we can use recently introduced methods\cite{Pagh2012},\cite{Charikar2004} of approximating the tensor product. These methods make use of the Count Sketch algorithm first introduced by Chrikar et. al. in 2011\cite{Charikar2004}.

\input{CountSketch.tex}

\subsection{Tensor Sketching}
The count sketch algorithm can be used to provide count sketches of tensor products. We call these tensor sketches.
\begin{definition}[Tensor Sketch]
A tensor sketch is a Count Sketch of a tensor product.
\begin{equation}
TS_p(\bm{x})=CS(\bm{x}^{(p)})
\end{equation}
\end{definition}

Using lemma\ref{lem:CountSketchE} we see:
\begin{equation}
E[\langle TS_p(\bm{x}),TS_p(y) \rangle] = E[\langle CS(\bm{x}^{(p)}),CS(\bm{y}^{(p)})\rangle]=\left\langle \bm{x}^{(p)},\bm{y}^{(p)}\right\rangle
\end{equation}
combining this with lemma \ref{lem:TensorMap} and \ref{lem:TensorConstant} we get:
\begin{equation}
E[\langle TS_p(\bm{x}),TS_p(y) \rangle] = (\langle \bm{x},\bm{y}\rangle+c)^p 
\end{equation}

So $TS_p(\bm{x})$ provides an approximate mapping for the polynomial kernel as given in definition \ref{def:PolyKernel}.

\subsubsection{Fast convolution}

A recent paper by Pagh\cite{Pagh2012} introduces a fast technique for constructing tensor sketches through fast convolution of count sketches. The heart of this technique is polynomial multiplication through the Fast Fourier Transformation.

Returning to the polynomial representation of the Count Sketch in eq.\ref{eq:countsketch poly} we now consider the Fast Fourier Transformation of two such polynomials.
\begin{equation}
FFT(f_x(u))=\{f_x(\omega^0),f_x(\omega^1),\cdots,f_x(\omega^{D-1})\}=\{\sum^d_{i=1}s_1(i)x_i(\omega^0)^{h_1(i)},\cdots
\end{equation}

\begin{equation}
FFT(f_y(u))=\{f_y(\omega^0),f_y(\omega^1),\cdots,f_y(\omega^{D-1})\}=\{\sum^d_{j=1}s_2(j)y_i(\omega^0)^{h_2(j)},\cdots
\end{equation}

By taking the componentwise multiplication of the two we can get a vector with $D$ entries. 
\begin{equation}
FFT(f_x)*FFT(f_y)_D=\sum^d_{i,j}s_1(i)s_2(j)x_iy_j(\omega^D)^{h_1(i)+h_2(j)\bmod{D}}
\end{equation}
Notice that the exponents are constrained by a $\mod{D}$ to concentrate the sums to $D$ terms. By choosing hash functions $S(i,j)=s_1(i)s_2(j)$ and $H(i,j)=h_1(i)+h_2(j)\bmod{D}$ we can rewrite the entries as:
\begin{equation}
\label{eq:FFTMethod}
\sum^d_{i,j}S(i,j)x_iy_j(\omega^t)^{H(i,j)}
\end{equation}
At this point we consider a direct computation of the Count Sketch of $x\otimes y$  using $S(i,j)$ and $H(i,j)$:
\begin{equation}
f_{x\otimes y}(u)=\sum_{i,j}^dS(i,j)x_iy_ju^{H(i,j)}
\end{equation}
It is clear that the discrete fourier transform of the sketch yields the same entries as \ref{eq:FFTMethod}.
\begin{equation}
FFT(f_{x\otimes y}(u))=FFT(f_x(u))*FFT(f_y(u))
\end{equation}
So by performing the Fourier Inverse on \ref{eq:FFTMethod} we have a fast method of computing $CS(\bm{x}^{(2)})$ from two different Count Sketches of $x$. The method generalizes to sketching any $p$-level tensor product:

\begin{definition}
Fast computation of $p$-level tensor sketch.\\

Compute $p$ Count Sketches using $p$ different hash-functions $h(\bm{x}):\mathbb{N}\rightarrow[D]$ and $s(\bm{x}):\mathbb{N}\rightarrow{\pm1}$. Now using component-wise multiplication compute the p-level tensor sketch as:
\begin{equation}
CS(\bm{x}^{(p)}) = FFT^{-1}(FFT(CS_1(\bm{x}))*\cdots*(FFT(CS_p(\bm{x})))
\end{equation}

Corresponding to $CS(\bm{x}^{(p)})$ using hash-functions $H$ and $S$ as:
\begin{equation}
H(i_1,...,i_p)=\sum_{k=1}^ph_k(i_k)\mod{D}
\end{equation}
\begin{equation}
S(i_1,...,i_p)=\prod_{k=1}^ps_k(i_k)
\end{equation}
\end{definition}
Since computing the Count Sketch is $O(d)$ and FFT is $O(D log D)$ this technique delivers a total runtime in $O(p(d+ D log D))$. \label{header:TSspeed}

\subsubsection{Error bounds}\label{TS:errorBound}

Approximation improves with $D$. The variance of the mapping is bounded by:
\begin{equation}
  \label{eq:TensorSketchVar}
  Var[\langle CS(\bm{x}^{(p)}), CS(\bm{y}^{(p)})\rangle] \leq \frac{1}{D}(\langle \bm{x},\bm{y} \rangle ^{2p} + ||x||^{2p}||y||^{2p})
\end{equation}

See \cite{Pham2013} for a proof of the bound.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 
